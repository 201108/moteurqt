#ifndef CYLINDRE_H
#define CYLINDRE_H

#include <QVector>
#include <GL/glu.h>
#include <QDebug>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>

class Cylindre
{
public:
    double ep_cyl;
    double rayon;
    long nb_facettes;
    int coul_r, coul_v, coul_b;
    int debut;
    int * end_tab;

public:
    Cylindre(double ep_cyl,
             double rayon,
             long nb_facettes,
             int coul_r, int coul_v,
             int coul_b,
             QVector<GLfloat> *vertices,
             QVector<GLfloat> *colors,
             int &index
             );

public:
    int dessiner_cylindre(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform);
    int * generate_cylindre(QVector<GLfloat> *vertices, QVector<GLfloat> *colors);

private:
    int dessiner_cote(QVector<GLfloat> *vertices, QVector<GLfloat> *colors);
    int dessiner_contour(QVector<GLfloat> *vertices, QVector<GLfloat> *colors);
};

#endif // CYLINDRE_H
