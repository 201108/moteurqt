#ifndef MOTEUR_H
#define MOTEUR_H

#include "vilbrequin.h"
#include "piston.h"
#include "culasse.h"
#include <QOpenGLBuffer>

#define pushMatrix(copy, matrix) copy = matrix; m_program->setUniformValue(m_matrixUniform, copy);
#define pullMatrix(copy,matrix) m_program->setUniformValue(m_matrixUniform, matrix);

class Moteur
{
public:
    Moteur();
    Moteur(QVector<GLfloat> *vertices,
           QVector<GLfloat> *colors,
           int &index);
    void draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform, double angle_vilbrequin, QOpenGLBuffer &m_vbo);

public:
    Vilbrequin vil_brequin;
    Piston pi1,pi2,pi3,pi4;
    Culasse c1,c2,c3,c4;
};

#endif // MOTEUR_H
