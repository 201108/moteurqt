#include "moteur.h"
#include <QtMath>

inline float squareQuarterRadius(float r){
    double d = qSqrt(2*(r*r))/2;
    return qSqrt((r*r)-(d*d));
}

Moteur::Moteur(QVector<GLfloat> *vertices,
               QVector<GLfloat> *colors,
               int &index):
    vil_brequin(vertices, colors, index),
    pi1(vertices, colors,index),
    pi2(vertices, colors,index),
    pi3(vertices, colors,index),
    pi4(vertices, colors,index),
    c1(Culasse(0.2, 0.05, 0.07, 12, 75, 125,11,126, vertices, colors,index)),
    c2(Culasse(0.2, 0.05, 0.07, 12, 75, 125,170,126, vertices, colors, index)),
    c3(Culasse(0.2, 0.05, 0.07, 12, 75, 125,44,126, vertices, colors, index)),
    c4(Culasse(0.2, 0.05, 0.07, 12, 75, 125,200,126, vertices, colors, index))
{}


void Moteur::draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform,double angle_vilbrequin, QOpenGLBuffer & m_vbo){
    QMatrix4x4 copy;
    angle_vilbrequin = angle_vilbrequin - (M_PI*2)*((int)(angle_vilbrequin/(M_PI*2)));


    pushMatrix(copy, matrix);
        copy.rotate(qRadiansToDegrees(angle_vilbrequin),1,0,0);
        vil_brequin.draw(m_program, copy, m_matrixUniform);
    pushMatrix(copy, matrix);

    float a = squareQuarterRadius(vil_brequin.bloc0.disqueG.rayon);
    float xA = qCos(angle_vilbrequin)*0.05;
    float yA = qSin(angle_vilbrequin)*0.05;
    float xB = qCos(angle_vilbrequin+M_PI)*0.05;
    float yB = qSin(angle_vilbrequin+M_PI)*0.05;

    double ep = vil_brequin.bloc0.ep_cycl;

    pushMatrix(copy, matrix);
        copy.translate(a*2,xA,yA);
        pi1.drawPiston(m_program, copy, m_matrixUniform,angle_vilbrequin);
    pushMatrix(copy, matrix);

    pushMatrix(copy, matrix);
        copy.translate(ep+a*2,xB,yB);
        pi1.drawPiston(m_program, copy, m_matrixUniform,angle_vilbrequin+M_PI);
    pushMatrix(copy, matrix);

    pushMatrix(copy, matrix);
        copy.translate(2*ep+a*2,xB,yB);
        pi1.drawPiston(m_program, copy, m_matrixUniform,angle_vilbrequin+M_PI);
    pushMatrix(copy, matrix);

    pushMatrix(copy, matrix);
        copy.translate(3*ep+a*2,xA,yA);
        pi1.drawPiston(m_program, copy, m_matrixUniform, angle_vilbrequin);
    pushMatrix(copy, matrix);



    //Hauteur maximale du piston
    double height = +qSin(M_PI/2)*0.05 +  pi1.bielle.ep_cyl + pi1.connecteur.ep_cyl/2 + pi1.tete.ep_cyl/2 - c1.ep_cyl/4 + c1.ep_cyl/10.0;

    QVector<GLfloat> * aze;
    aze = new QVector<GLfloat>();
    pushMatrix(copy, matrix);
        copy.rotate(90,1,0,0);
        copy.translate(a*2,0,-height);
        copy.rotate(90,0,0,1); 
        c1.ignite(angle_vilbrequin);
        c1.dessiner_contour(aze, aze, c1.rayonI);
        m_vbo.write(c1.debut*11*sizeof (GLfloat), aze->constData(), aze->length()*sizeof (GLfloat));
        c1.draw(m_program, copy, m_matrixUniform);
    pushMatrix(copy, matrix);
    aze = new QVector<GLfloat>();
    pushMatrix(copy, matrix);
        copy.rotate(90,1,0,0);
        copy.translate(ep+a*2,0,-height);
        copy.rotate(90,0,0,1);
        c2.ignite(angle_vilbrequin+M_PI);
        c2.dessiner_contour(aze, aze, c2.rayonI);
        m_vbo.write(c2.debut*11*sizeof (GLfloat), aze->constData(), aze->length()*sizeof (GLfloat));
        c2.draw(m_program, copy, m_matrixUniform);
    pushMatrix(copy, matrix);
    aze = new QVector<GLfloat>();
    pushMatrix(copy, matrix);
        copy.rotate(90,1,0,0);
        copy.translate(2*ep+a*2,0,-height);
        copy.rotate(90,0,0,1);
        c3.ignite(angle_vilbrequin+M_PI);
        c3.dessiner_contour(aze, aze, c3.rayonI);
        m_vbo.write(c3.debut*11*sizeof (GLfloat), aze->constData(), aze->length()*sizeof (GLfloat));
        c3.draw(m_program, copy, m_matrixUniform);
    pushMatrix(copy, matrix);
    aze = new QVector<GLfloat>();
    pushMatrix(copy, matrix);
        copy.rotate(90,1,0,0);
        copy.translate(3*ep+a*2,0,-height);
        copy.rotate(90,0,0,1);
        c4.ignite(angle_vilbrequin);
        c4.dessiner_contour(aze, aze, c4.rayonI);
        m_vbo.write(c4.debut*11*sizeof (GLfloat), aze->constData(), aze->length()*sizeof (GLfloat));
        c4.draw(m_program, copy, m_matrixUniform);
    pushMatrix(copy, matrix);
}
