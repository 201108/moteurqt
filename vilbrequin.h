#ifndef VILBREQUIN_H
#define VILBREQUIN_H


#define pushMatrix() copy = matrix; m_program->setUniformValue(m_matrixUniform, copy);
#define pullMatrix() m_program->setUniformValue(m_matrixUniform, matrix);

#include <QVector>
#include <GL/glu.h>
#include <QDebug>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include "cylindre.h"
#include "blocpiston.h"

class Vilbrequin
{
public:
    Vilbrequin(QVector<GLfloat> *vertices,
               QVector<GLfloat> *colors,
               int &index);
    void draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform);

public:
    BlocPiston bloc0, bloc1, bloc2, bloc3;
};

#endif // VILBREQUIN_H
