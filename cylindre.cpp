#include "cylindre.h"
#include <GL/glu.h>
#include <QtMath>
#include <QDebug>
#include <QOpenGLFunctions>

Cylindre::Cylindre(double ep_cyl,
                   double rayon,
                   long nb_facettes,
                   int coul_r, int coul_v,
                   int coul_b,
                   QVector<GLfloat> *vertices,
                   QVector<GLfloat> *colors,
                   int &index
                   )
    :ep_cyl(ep_cyl),
    rayon(rayon),
    nb_facettes(nb_facettes),
    coul_r(coul_r),
    coul_v(coul_v),
    coul_b(coul_b),
    debut(index)
{
    //qDebug() << "debut " << debut;
    end_tab = generate_cylindre(vertices, colors);
    int a = end_tab[4]+end_tab[5];
    index += a;
}

int Cylindre::dessiner_cote(QVector<GLfloat> *vertices, QVector<GLfloat> *colors){
    double megaPi = 2 * M_PI;
    vertices->append(0);
    vertices->append(0);
    vertices->append(0);
    //pos
    colors->append(0);
    colors->append(0);
    colors->append(0);

    vertices->append(coul_r / 255.);
    vertices->append(coul_v / 255.);
    vertices->append(coul_b / 255.);
    //tex
    colors->append(0.0);
    colors->append(0.0);
    //col
    colors->append(1);
    colors->append(1);
    colors->append(1);
    //normale
    colors->append(1);
    colors->append(1);
    colors->append(1);

    int var = 0;
    for (var; var <= nb_facettes; ++var) {
        vertices->append(rayon*qCos(var*megaPi/nb_facettes));
        vertices->append(rayon*qSin(var*megaPi/nb_facettes));
        vertices->append(0.0);
        //pos
        colors->append(rayon*qCos(var*megaPi/nb_facettes));
        colors->append(rayon*qSin(var*megaPi/nb_facettes));
        colors->append(0.0);

        vertices->append(coul_r / 255.);
        vertices->append(coul_v / 255.);
        vertices->append(coul_b / 255.);
        //tex
        colors->append(0.5); //
        colors->append(0.0); //revoir les coord des textures

        //col
        colors->append(1);
        colors->append(1);
        colors->append(1);

        //normale
        colors->append(1);
        colors->append(1);
        colors->append(1);
    }
    return nb_facettes+1;
}
/*
int Cylindre::dessiner_contour(QVector<GLfloat> *vertices, QVector<GLfloat> *colors){
    double megaPi = 2 * M_PI;
    int var = 0;

    for (var; var < nb_facettes; var++) {
        QVector3D aU(rayon*qCos(var*megaPi/nb_facettes),
                     rayon*qSin(var*megaPi/nb_facettes),
                     +0.5*ep_cyl);
        QVector3D aD(rayon*qCos(var*megaPi/nb_facettes),
                     rayon*qSin(var*megaPi/nb_facettes),
                     -0.5*ep_cyl);
        QVector3D nU(rayon*qCos((var+1)*megaPi/nb_facettes),
                     rayon*qSin((var+1)*megaPi/nb_facettes),
                     +0.5*ep_cyl);
        QVector3D nD(rayon*qCos((var+1)*megaPi/nb_facettes),
                     rayon*qSin((var+1)*megaPi/nb_facettes),
                     -0.5*ep_cyl);

        QVector3D face = QVector3D::normal(QVector3D(nU-aU),QVector3D(aD-aU));
        vertices->append(aU.x());
        vertices->append(aU.y());
        vertices->append(aU.z());

        colors->append(aU.x());
        colors->append(aU.y());
        colors->append(aU.z());

        vertices->append(coul_r / 255. * .5);
        vertices->append(coul_v / 255. * .5);
        vertices->append(coul_b / 255. * .5);

        colors->append(0.0);
        colors->append(0.0);

        //face = QVector3D::normal(QVector3D(aU-aD),QVector3D(nD-aD));
        colors->append(face.x());
        colors->append(face.y());
        colors->append(face.z());


        vertices->append(rayon*qCos(var*megaPi/nb_facettes));
        vertices->append(rayon*qSin(var*megaPi/nb_facettes));
        vertices->append(-0.5*ep_cyl);

        colors->append(aD.x());
        colors->append(aD.y());
        colors->append(aD.z());

        vertices->append(coul_r / 255. * .5);
        vertices->append(coul_v / 255. * .5);
        vertices->append(coul_b / 255. * .5);

        colors->append(1.0);
        colors->append(0.0);

        face = QVector3D::normal(QVector3D(aU-nU),QVector3D(nD-nU));
        colors->append(face.x());
        colors->append(face.y());
        colors->append(face.z());


        vertices->append(rayon*qCos((var+1)*megaPi/nb_facettes));
        vertices->append(rayon*qSin((var+1)*megaPi/nb_facettes));
        vertices->append(0.5*ep_cyl);

        colors->append(nU.x());
        colors->append(nU.y());
        colors->append(nU.z());

        vertices->append(coul_r / 255. * .5);
        vertices->append(coul_v / 255. * .5);
        vertices->append(coul_b / 255. * .5);

        colors->append(0.0);
        colors->append(1.0);

        //face = QVector3D::normal(QVector3D(nU-nD),QVector3D(aD-nD));
        colors->append(face.x());
        colors->append(face.y());
        colors->append(face.z());


        vertices->append(rayon*qCos((var+1)*megaPi/nb_facettes));
        vertices->append(rayon*qSin((var+1)*megaPi/nb_facettes));
        vertices->append(-0.5*ep_cyl);

        colors->append(nD.x());
        colors->append(nD.y());
        colors->append(nD.z());

        vertices->append(coul_r / 255. * .5);
        vertices->append(coul_v / 255. * .5);
        vertices->append(coul_b / 255. * .5);

        colors->append(1.0);
        colors->append(1.0);

        colors->append(face.x());
        colors->append(face.y());
        colors->append(face.z());
    }
    return (var) * 4;
}*/

int Cylindre::dessiner_contour(QVector<GLfloat> *vertices, QVector<GLfloat> *colors){
    double megaPi = 2 * M_PI;
    int var = 0;

        QVector3D pU(rayon*qCos((nb_facettes-1)*megaPi/nb_facettes),
                     rayon*qSin((nb_facettes-1)*megaPi/nb_facettes),
                    +0.5*ep_cyl);
        /*QVector3D pD(rayon*qCos((nb_facettes-1)*megaPi/nb_facettes),
                     rayon*qSin((nb_facettes-1)*megaPi/nb_facettes),
                     -0.5*ep_cyl);*/
    for (var; var < nb_facettes; var++) {
        QVector3D aU(rayon*qCos(var*megaPi/nb_facettes),
                     rayon*qSin(var*megaPi/nb_facettes),
                     +0.5*ep_cyl);
        QVector3D aD(rayon*qCos(var*megaPi/nb_facettes),
                     rayon*qSin(var*megaPi/nb_facettes),
                     -0.5*ep_cyl);
        QVector3D nU(rayon*qCos((var+1)*megaPi/nb_facettes),
                     rayon*qSin((var+1)*megaPi/nb_facettes),
                     +0.5*ep_cyl);
        QVector3D nD(rayon*qCos((var+1)*megaPi/nb_facettes),
                     rayon*qSin((var+1)*megaPi/nb_facettes),
                     -0.5*ep_cyl);

        QVector3D pNorm = QVector3D::normal(QVector3D(pU-aU), QVector3D(aD-aU));

        QVector3D nNorm = QVector3D::normal(QVector3D(nU-aU),QVector3D(aD-aU));

        QVector3D phong = (pNorm+nNorm)/2;

        colors->append(aU.x());
        colors->append(aU.y());
        colors->append(aU.z());

        colors->append(0.0);
        colors->append(0.0);

        //face = QVector3D::normal(QVector3D(aU-aD),QVector3D(nD-aD));
        colors->append(1);
        colors->append(1);
        colors->append(1);

        colors->append(phong.x());
        colors->append(phong.y());
        colors->append(phong.z());


        colors->append(aD.x());
        colors->append(aD.y());
        colors->append(aD.z());

        colors->append(1.0);
        colors->append(0.0);

        colors->append(1);
        colors->append(1);
        colors->append(1);

        //face = -QVector3D::normal(QVector3D(aU-nU),QVector3D(nD-nU));
        colors->append(phong.x());
        colors->append(phong.y());
        colors->append(phong.z());


        colors->append(nU.x());
        colors->append(nU.y());
        colors->append(nU.z());

        colors->append(0.0);
        colors->append(1.0);

        colors->append(1);
        colors->append(1);
        colors->append(1);

        //face = QVector3D::normal(QVector3D(nU-nD),QVector3D(aD-nD));
        colors->append(nNorm.x());
        colors->append(nNorm.y());
        colors->append(nNorm.z());


        colors->append(nD.x());
        colors->append(nD.y());
        colors->append(nD.z());

        colors->append(1.0);
        colors->append(1.0);

        colors->append(1);
        colors->append(1);
        colors->append(1);

        colors->append(nNorm.x());
        colors->append(nNorm.y());
        colors->append(nNorm.z());
    }
    return (var) * 4;
}

int * Cylindre::generate_cylindre(QVector<GLfloat> *vertices, QVector<GLfloat> *colors){
    int debut = vertices->length();
    int shape1 = dessiner_cote(vertices, colors);
    int shape2 = dessiner_cote(vertices, colors);
    int shape3 = dessiner_contour(vertices, colors);

    int * tab = new int[6];
    tab[0] = 0;
    tab[1] = shape1 + 1;
    tab[2] = shape1 + 1;
    tab[3] = shape2 + 1;
    tab[4] = shape1 + shape2 + 2;
    tab[5] = shape3;

    return tab;
}

int Cylindre::dessiner_cylindre(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform){
    QMatrix4x4 copy = matrix;

    copy.translate(0, 0, .5 * ep_cyl);
    m_program->setUniformValue(m_matrixUniform, copy);
    glDrawArrays(GL_TRIANGLE_FAN, debut+end_tab[0], end_tab[1]);
    m_program->setUniformValue(m_matrixUniform, matrix);

    copy = matrix;
    copy.translate(0, 0, -.5 * ep_cyl);
    m_program->setUniformValue(m_matrixUniform, copy);
    glDrawArrays(GL_TRIANGLE_FAN, debut+end_tab[2], end_tab[3]);
    m_program->setUniformValue(m_matrixUniform, matrix);

    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[4], end_tab[5]);

}
