attribute highp vec4 posAttr;
attribute mediump vec4 texAttr;
attribute mediump vec4 colAttr;
attribute highp vec3 norAttr;
varying highp  vec4 col;
varying mediump vec4 texc;
varying highp vec3 nor;
uniform highp mat4 matrix;
uniform highp mat4 mvMatrix;
uniform highp mat3 norMatrix;

void main() {
   col = colAttr;
   texc = texAttr;
   gl_Position = matrix * posAttr;
   nor = norMatrix * norAttr;
}



