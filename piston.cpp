#include "piston.h"


inline float squareQuarterRadius(float r){
    double d = qSqrt(2*(r*r))/2;
    return qSqrt((r*r)-(d*d));
}

Piston::Piston(QVector<GLfloat> *vertices,
               QVector<GLfloat> *colors,
               int &index):
    tete(Cylindre(0.1, 0.05, 12, 125,124,126, vertices, colors, index)),
    bielle(Cylindre(bieleLength(),0.01,400,120,112,128,vertices, colors, index)),
    connecteur(Cylindre(0.03,0.04,4,250,245,255,vertices, colors, index))
{}

void Piston::drawPiston(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform, double angle){

    QMatrix4x4 copy = matrix;
    float xA = qSin(angle)*0.05;
    double longueur = bielle.ep_cyl+connecteur.ep_cyl/2;
    double height = qSqrt( (longueur*longueur)-(xA*xA) );

    pushMatrix(copy, matrix);
        copy.translate(0,height+tete.ep_cyl/2,-xA);
        copy.rotate(-90,1,0,0);
        tete.dessiner_cylindre(m_program, copy, m_matrixUniform);
    pullMatrix(copy, matrix);

    double alpha = -qRadiansToDegrees(qAsin(xA/longueur));

    pushMatrix(copy, matrix);
        copy.rotate(90,0,1,0);
        copy.rotate(45+alpha,0,0,1);
        connecteur.dessiner_cylindre(m_program,copy,m_matrixUniform);
    pullMatrix(copy, matrix);

    pushMatrix(copy, matrix);
        copy.rotate(90,0,1,0);
        copy.rotate(alpha,0,0,1);
        copy.translate(0,connecteur.ep_cyl/2+bielle.ep_cyl/2,0);
        copy.rotate(90,1,0,0);
        bielle.dessiner_cylindre(m_program,copy,m_matrixUniform);
    pullMatrix(copy, matrix);


    /*
    QMatrix4x4 cop2;
    pushMatrix(copy, matrix);
        copy.rotate(-90,0,1,0);
        copy.rotate(alpha,0,0,1);
        pushMatrix(cop2,copy);
            cop2.rotate(90,1,0,0);
            //cop2.translate(xa,0,-(azer+bieleLength()+tete.ep_cyl/2));
            cop2.rotate(-alpha,0,1,0);
            tete.dessiner_cylindre(m_program, cop2, m_matrixUniform);
        pullMatrix(cop2,copy);

        pushMatrix(cop2,copy);
            cop2.rotate(90,1,0,0);
            cop2.translate(0,0,-(azer+bieleLength()/2));
            bielle.dessiner_cylindre(m_program,cop2, m_matrixUniform);
        pullMatrix(cop2,copy);

        copy.rotate(45,0,0,1);
        connecteur.dessiner_cylindre(m_program,copy,m_matrixUniform);
    pullMatrix(copy,matrix);*/

}
