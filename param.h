#ifndef PARAM_H
#define PARAM_H

#include <QWidget>

namespace Ui {
class param;
}

class param : public QWidget
{
    Q_OBJECT

public:
    explicit param(QWidget *parent = nullptr);
    ~param();

signals:
    void distanceChanged(int val);
    void radiusChanged(double val);
    void nearChanged(int val);
    void farChanged(int val);
    void angleChanged(int val);
    void rpmChanged(int val);
    void lumiereChanged(int val);
    void turnedOn();
    void turnedOff();

public slots:
    void setDistance(double val);
    void setRadius(double val);
    void setNear(double val);
    void setFar(double val);
    void setAngle(double val);
    void setRpm(int val);
    void turnOn();
    void turnOff();

private:
    Ui::param *ui;
};

#endif // PARAM_H
