// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#define pushMatrix() copy = matrix; m_program->setUniformValue(m_matrixUniform, copy);
#define pullMatrix() m_program->setUniformValue(m_matrixUniform, matrix);

#include "glarea.h"

#include <GL/glu.h>
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>


static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";


GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
                        // cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    m_timer = new QTimer(this);
    m_timer->setInterval(50);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));
    setFar(5);setNear(1);setDistance(3);
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete m_timer;

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();

    // ici destructions de ressources GL
    tearGLObjects();

    doneCurrent();
}


void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);
    glEnable (GL_TEXTURE_2D);
    makeGLObjects();

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

    // récupère identifiants de "variables" dans les shaders
    /*m_posAttr = m_program->attributeLocation("posAttr");
    m_colAttr = m_program->attributeLocation("colAttr");
    m_norAttr = m_program->attributeLocation("norAttr");
    m_matrixUniform = m_program->uniformLocation("projMatrix");*/

    m_program->setUniformValue("texture", 0);

}

void GLArea::makeGLObjects(){

    //m = Moteur(vertices, colors, index);

    /*GLfloat * vert_arr = new GLfloat[vertices->size()];
    GLfloat * col_arr = new GLfloat[colors->size()];*/
    //for(int i  = 0; i < vertices->size(); i++) { vert_arr[i] = vertices->value(i); }
    //for(int i = 0; i < colors->size(); i++){ col_arr[i] = colors->value(i); }

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(colors->constData(), colors->count() * sizeof(GLfloat));

    //m_vbo.write(12, colors->constData() ,colors->length()/2*sizeof(GLfloat));
    QString nom = QString(":/tex.png");
    QImage image(nom);
    m_textures[0] = new QOpenGLTexture(image);
}

void GLArea::tearGLObjects(){
    m_vbo.destroy();
    delete m_textures[0];
}

void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
    // doProjection();
}

void GLArea::paintGL()
{
    qDebug() << __FUNCTION__ ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program

    QMatrix4x4 matrix;
    GLfloat hr = m_radius, wr = hr * m_ratio;            // = glFrustum
    matrix.frustum(-wr, wr, -hr, hr, near, far);
    //matrix.perspective(60.0f, m_ratio, 0.1f, 100.0f);  // = gluPerspective

    // Remplace gluLookAt (0, 0, 3.0, 0, 0, 0, 0, 1, 0);
    matrix.translate(0, 0, -distance);

    // Rotation de la scène pour l'animation
    matrix.rotate(m_angle-60,0,1,0);

    QMatrix4x4 cam_mat;
    //cam_mat.translate(0, 0, -3.0);

    QMatrix4x4 world_mat;
    world_mat.rotate(angle_lumiere-90, 0, 1, 0);

    QMatrix3x3 normal_mat = world_mat.normalMatrix();

    m_program->setUniformValue("projMatrix", matrix);
    m_program->setUniformValue("mvMatrix", cam_mat*world_mat);
    m_program->setUniformValue("norMatrix", normal_mat);

    m_program->setUniformValue(m_matrixUniform, matrix);

    float y = 0.9-m_anim;

    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3, 11 * sizeof(GLfloat));
    m_program->setAttributeBuffer("texAttr", GL_FLOAT, 3 * sizeof(GLfloat), 2, 11 * sizeof(GLfloat));
    //m_program->setAttributeBuffer("texAttr", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 5 * sizeof(GLfloat), 3, 11 * sizeof(GLfloat));
    m_program->setAttributeBuffer("norAttr", GL_FLOAT, 8 * sizeof(GLfloat), 3, 11 * sizeof(GLfloat));

    m_program->enableAttributeArray("posAttr");
    m_program->enableAttributeArray("texAttr");
    m_program->enableAttributeArray("colAttr");
    m_program->enableAttributeArray("norAttr");

    m_textures[0]->bind();

    m.draw(m_program, matrix, m_matrixUniform,m_alpha, m_vbo);
    //c.dessiner_cylindre(m_program, matrix, m_matrixUniform);
    m_textures[0]->release();

    m_program->disableAttributeArray("posAttr");
    m_program->disableAttributeArray("texAttr");
    m_program->disableAttributeArray("colAttr");
    m_program->disableAttributeArray("norAttr");


    m_program->release();
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
    switch(ev->key()){
        case Qt::Key_Space :
            setAngle(m_angle+1);
            break;
        case Qt::Key_D:
            turnOn();
            break;
        case Qt::Key_S:
            turnOff();
            break;
        case Qt::Key_A :
            if(ev->text() == "a"){
                setRpm(rpm-50);
            }else{
                setRpm(rpm+50);
            }
            break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void GLArea::onTimeout()
{
    qDebug() << __FUNCTION__ ;
    m_alpha+=(M_PI*2*rpm)/3600.0;
    //m_anim += 0.01;
    if (m_anim > 1) m_anim = 0;
    update();
}

void GLArea::turnOn(){
    if(!isStarted){
        m_timer->start();
        isStarted=true;
        emit turnedOn();
    }
}

void GLArea::turnOff(){
    if(isStarted){
        m_timer->stop();
        isStarted=false;
        emit turnedOff();
    }
}

void GLArea::setRpm(int rpm){
    if(rpm >=40 && rpm <=7000 && rpm != this->rpm){
        this->rpm = rpm;
        //update();
        emit rpmChanged(rpm);
    }
}

void GLArea::setDistance(double val){
    if(distance != val){
        distance = val;
        update();
        emit distanceChanged(val);
    }
}

void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        update();
    }
}

void GLArea::setNear(double val){
    if(near != val){
        if(val > far){val=far-0.1;}
        near = val;
        update();
        emit nearChanged(val);
    }
}

void GLArea::setFar(double val){
    if(far != val){
        if(val < near){val=near+0.1;}
        far = val;
        update();
        emit farChanged(val);
    }
}

void GLArea::setAngle(double val){
    if(m_angle != val){
        if (val >= 360) val -= 360;
        m_angle = val;
        update();
        emit angleChanged(val);
    }
}

void GLArea::setLumiere(int val){
    qDebug() << "lumi" << val;
    if(val>360 || val <0){
        val = 0;
    }
    angle_lumiere = val;
    update();
}

