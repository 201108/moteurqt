// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef PRINC_H
#define PRINC_H

#include "ui_princ.h"
#include "param.h"

class Princ : public QMainWindow, private Ui::Princ
{
    Q_OBJECT
    param set;

public:
    explicit Princ(QWidget *parent = 0);

public slots:
    void openSettings();

};

#endif // PRINC_H
